\chapter{Iput/Output models}\label{c:io-models}
Communicating with external sources is essential in any program. In order to implement this communication efficiently, a deeper understanding of the operating system and how it can execute input and output operations is essential. In this chapter I am going to introduce a few ways the Linux operating system can perform IO.
\par
There are several different IO operations such as writing, opening and closing a file, but I am only going to describe reading for the sake of brevity. These other operations are conceptually similar, but also very different in their specifics. Described in a very abstract manner, a read operation consist of the following steps:

\begin{itemize}
\item
  A user process executes a system call on a file descriptor, thus switching to kernel mode
\item
  If the requested amount of data is already available in the kernel's buffers, it is copied to the user's memory space. Otherwise the kernel needs to fetch data from the source underlying the file descriptor. This may require communication with a hard disk, a network interface, or a keyboard controller. These operations may take time several orders of magnitude longer than a CPU instruction. In the case of reading from a keyboard this operation may even take an indefinite amount of time.
\item
  The kernel call returns, and the user program continues to run in user mode
\end{itemize}

The order, timing and data structures involved in these steps distinguish between different IO models. Next I am going to describe these characteristics of the different methods. I am only going to detail the case where the kernel doesn't have sufficient data in it's buffers. Linux provides a unified file descriptor interface to cover multiple IO devices, but in the following chapters I am going to use a TCP socket as an example.

\section{Blocking}\label{s:blocking}

\subsection{Overview}

Our goal is to serve many users at the same time. The Linux system offers processes to implement running multiple tasks concurrently on a limited number of processors, as if these tasks were running in parallel. One of the simplest solutions is to create a process to serve each user. The blocking IO model is perfectly suited for this. The name ``blocking IO'' refers to the fact that processes invoking these functions are idle while the operation is being performed.

\begin{enumerate}
\item
  The user instructs the kernel to read from a file descriptor.
\item
  The calling process is put into a sleeping state, and it is inserted into a wait queue.
\item
  The kernel, through it's networking layer instructs a network interface, to read data.
\item
  The device finishes reading, copies data into the main memory, and notifies the kernel.
\item
  The kernel copies the data to the user's memory space, and notifies members of the wait queue for this event. The process is put into a runnable state, and once scheduled, it will run in user mode.
\end{enumerate}

\begin{figure}[h]
\centering
\includegraphics[scale=0.7]{figures/blocking-read}
\caption{Sequence diagram of the blocking IO model}
\label{fig:blocking-read}
\end{figure}

\section{Non-blocking}\label{non-blocking}

\subsection{Using a single file descriptor}

Reading from sockets are done by system calls using file descriptors. Prior to the call the file descriptor can be set to non-blocking mode. In this mode IO operations will be executed differently.

\begin{enumerate}
\item
  The user enables non-blocking operations on the file descriptor.
\item
  The user instructs the kernel to read from a file descriptor.
\item
  The requested data is not ready, so the kernel starts to fetch it. If the file descriptor was not set to non-blocking mode, it would need to be put into a wait queue, and notified when the data is ready. Instead of blocking, the call results in error code \texttt{EWOULDBLOCK}, and it can continue to run, but it will not be notified when the data is ready.
\end{enumerate}

Calling read again would result in the same error code, until the kernel is done fetching the data, after which it would signal success, and fill the user's buffer with the requested data.
\par
To find out when the data is available, the user could check the descriptor periodically. This is called polling. There are certain tradeoffs related to polling:

\begin{itemize}
\item
  Polling at short intervals results in quicker response time, but consumes a lot of CPU time
\item
  Polling at long intervals does not require as much CPU time, but it increases latency
\end{itemize}

It is also an issue that each read call requires the process to switch to kernel mode, which would be wasteful when performed frequently, on a lot of files. This method of polling a file is almost never used, because there are convenient kernel methods that address this issue.

\subsection{Readiness change notification}\label{s:readiness-notification}

The Linux kernel offers a few different methods to notify processes when one or more of a set of files has become ready for an operation that the caller is interested in performing on a file descriptor. This way the operating system takes care of the polling. The kernel has direct access to the file descriptor's underlying driver, therefore it doesn't need to check the file descriptor periodically, it can ask the driver to register the wait queues associated with the events for which the user showed interest. Once the driver receives some data, it notifies members of the wait queue that the event has occurred.
\par
Readiness notification supported by the kernel is much more efficient than userspace polling. No CPU cycles are wasted periodically checking the status of each file, and there is no issue of having to choose a satisfactory frequency.
\par
Programs using this IO method often set up a structure called the event loop to handle readiness change events. A pseudo-code example of an event loop is provided in listing \ref{lst:event_loop}.

\begin{listing}
\caption{An event loop based on readiness notification}
\label{lst:event_loop}
\begin{minted}[linenos]{C}
void event_loop() {
  interests = set_up_initial_interests();
  while(true) {
    events = wait_for_readiness(interests);
    foreach(event) {
    	handle_event(event);
    }
  }
}

void handle_event(event) {
  if(event.type == EV_READ) {
    message = process_data(event.file_descriptor);
    if(message_not_over) {
      event.channel.read_data(); 
      //Declares an interest in reading the file descriptor again
    } else {
      event.channel.write_data(response);  	
      //Declares an interest in writing the file descriptor
    }
  } else {
    ...
  }
}
\end{minted}
\end{listing}
\begin{figure}[h]
\centering
\includegraphics[scale=0.7]{figures/readiness-notification-read}
\caption{Sequence diagram of the readiness notification IO model}
\label{fig:readiness-notification-read}
\end{figure}
Using a single thread with readiness notification instead of separate threads may seem very different, but they are also similar in some ways. In both cases the kernel sets up a data structure through which it can communicate with the user, to signal that reading is ready. In the case of readiness notification this communication is very explicit. On the other hand, using a thread per connection, the notification is hidden away from the user. The process doesn't need to call a function to wait for the operation to complete, it is put on a wait queue by the kernel, and woken up once the operation is done.
There are a few different system calls that provide readiness notification capabilities. In the next sections I am going to briefly introduce them, and compare their characteristic features.

\subsubsection{select}

\begin{listing}
\caption{The select system call}
\label{lst:select}
\begin{minted}[linenos]{C}
int select(int nfds, fd_set *readfds, fd_set *writefds, fd_set *exceptfds, 
    struct timeval *timeout);
\end{minted}
\end{listing}

To declare events of interest, the user populates and passes three different file descriptor sets, for read, write and exceptional events respectively. After calling the function, the process is blocked, and it will be woken up when one or more of the events occurred, or the specified amount of time expired. The return value is equal to the number of events that occurred. The file descriptor sets are modified. They may be checked sequentially to decide which file descriptors have events associated with them. 
\par
Select was the first standard system call to support readiness notification, and it is unsuitable to handle massive workloads. This shortcoming is evident considering that the upper limit of the number of file descriptors is 1024 by default. As the file descriptor sets are modified on return, they must be prepared before each call. This problem might be mitigated by keeping so-called shadow copies of the file descriptor sets, which are maintained in parallel with the ones passed to the method. The number of event types that select can monitor is very limited, and the set of monitored descriptors cannot be changed while the select call is in progress.
\par
Even if we don't consider the default limit of 1024 file descriptors, having to check each one of them every time the select call returns takes a number of steps that is proportional to the number of elements in our interest sets. The kernel also has to take a similar number of steps to check files for readiness, and set up monitoring for them, as each select call may have different interest sets.

\subsubsection{poll}

\begin{listing}
\caption{The poll system call}
\label{lst:poll}
\begin{minted}[linenos]{C}
int poll(struct pollfd *fds, nfds_t nfds, int timeout);

struct pollfd {
    int fd;         
    short events;
    short revents;
};
\end{minted}
\end{listing}

The poll system call requires its user to set up an array of pollfd structures. Each one contains the file descriptor, a bitmask for events that need to be monitored. Upon return, each pollfd structure's \texttt{revents} fields contain the operations that are available for execution without blocking on the file descriptor.
\par
This call has no artificial upper limit on the number of file descriptors it can monitor. Using poll, file descriptors can be monitored for more than three kinds of events, and the pollfd objects can be reused, so there's no need to keep shadow copies. Poll improves a lot on the usability issues of select, but the set of monitored events still can not be manipulated while the call is in progress.
\par
The scalability issue remains. All of the descriptors have to be checked one by one on return, regardless of how many events occurred, so the checking step scales linearly with the number of monitored file desctiptors. If a large portion of the file descriptors are inactive, this is a lot of useless work. The kernel has to take a similar number of steps to check and set up the tracking of these events, as the interest set may change between subsequent calls.

\subsubsection{epoll}

\begin{listing}
\caption{The epoll function family}
\label{lst:epoll}
\begin{minted}[linenos]{C}
int epoll_create1(int flags);

int epoll_ctl(int epfd, int op, int fd, struct epoll_event *event);
typedef union epoll_data {
   void        *ptr;
   int          fd;
   uint32_t     u32;
   uint64_t     u64;
} epoll_data_t;
struct epoll_event {
   uint32_t     events;
   epoll_data_t data;
};

int epoll_wait(int epfd, struct epoll_event *events, 
	int maxevents, int timeout);

\end{minted}
\end{listing}
These three system calls form the epoll (event poll) interface. \texttt{epoll\_create1} is used to set up the epoll structure (which is also a file descriptor). The \texttt{epoll\_ctl} control method modifies the epoll structure by adding or removing events from the interest set. The final function, \texttt{epoll\_wait} blocks the calling process until some events in the interest set become available. Upon return the \texttt{events} parameter points to an array of at most \texttt{maxevents} pieces of structures that represent files which  are either:
\begin{itemize}
\item
  Available for the designated operation without blocking (level triggered mode).
\item
  Became available for the designated operation since the last call (edge triggered mode).
\end{itemize}
The epoll structure can be modified while an operation is waiting on it using the epoll\_wait method, and it does not have an artificial limit for the maximum number of monitored file descriptors.
\par
Iterating over the result set scales well with the number of file descriptors in the interest set. \texttt{epoll\_wait} only returns information about the file descriptors which are ready, compared to poll, using which the program has to iterate over the whole interest set. Calling epoll\_wait is also relatively cheap, as the kernel doesn't have to set up all of the file descriptors for each call, as it is always informed of the user's interest set via epoll\_ctl. A downside of these methods is that changing the interest set requires a system call (which involves switching to kernel mode), and it modifies the internal representation of the epoll structure, a red-black tree. The red-black tree has an item for each element of the interest set, so modifying it costs a number of steps proportional to the depth of the red-black tree ($O(\log{|interest\_set|})$).

\subsection{Signal-driven (not covered)}
For the sake of completeness, I will mention signal-driven IO. This method is similar to the epoll model. Interests can be declared to the kernel, and the user has a method to wait for file descriptors to become ready. The difference is that in this case the events are signals delivered by the kernel. This method is much less popular, although the reasons for it is unclear. I have not found sources on the history of how epoll-like interfaces became the more popular way to handle non-blocking IO.

\section{Asynchronous}
The observant reader might have noticed that in the introduction I advocated for asynchronous processing, but none of the above methods are asynchronous. These functions are not meant to read asynchronously, but rather  to wait for file descriptors to become ready for reading. Once the events arrived, the otherwise blocking read will not block unnecessarily\footnote{It is technically still advisable to read the descriptor in a non-blocking fashion, as something might have occurred that rendered the available data unusable - such as another process reading it.}. That is to say that the thread will not be put on a wait queue, and rescheduled. The read method still needs to copy data from kernel buffers to userspace, and the calling thread needs for this function call to complete, just as any other blocking method call. Non-blocking IO is instead used by libraries to simulate asynchronous operations.
\par
To understand how the asynchronous IO model differs from readiness notification, compare figure(\ref{fig:async-read}) and figure(\ref{fig:readiness-notification-read}). Using readiness notification, the user is only notified that data is ready, then has to issue another command to read the results. In contrast, when the kernel notifies the user that an asynchronous operation is complete, the data is already in the user's buffer. This could be easily achieved by spawning a new thread for each read operation, and performing the blocking read on that thread. This would of course go against the main goal of async frameworks, which is to reduce the number of threads used. 
\begin{figure}[h]
\centering
\includegraphics[scale=0.7]{figures/async-read}
\caption{Sequence diagram of the asynchronous IO model}
\label{fig:async-read}
\end{figure}
\par
Linux implements the POSIX AIO interface, which is intended to describe truly asynchronous IO operations. Unfortunately according to the Linux implementation's manual pages\cite{posix-aio} it is currently implemented in userspace, using multiple threads, therefore it's not feasible for our purposes.