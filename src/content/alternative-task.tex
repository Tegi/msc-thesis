\chapter{Alternative task implementations}\label{c:alternative-task}
In this chapter I will review methods for task implementation other than 
the one provided by a single thread per task. For each alternative I will examine
if they provide the same functionalities that threads provided, and how these functionalities
are achieved.
\par
The specifics of how task state management and scheduling are realized will reveal
why these alternatives can be faster than threads. I've identified two main performance
issues of the thread per task model: inefficient state management and overly general
--- and thereby complicated --- scheduling. It is not surprising that these alternative 
methods will have to improve on these two areas.
\par
I will also demonstrate how the sample program in listing \ref{lst:thread-example}
may be implemented using each of the alternative models, and how these implementations 
compare to the thread based model. Whether the different programming models are
better or worse is largely opinionated. However --- due to the immense popularity
of threads --- I think a programming model similar to the thread per task method
should be easier to learn.

\section{Nested callbacks}
Using callbacks to notify processes of IO completion without blocking is an idea I 
have already visited in this thesis.
Recall how the operating system handles networking operations:
\begin{enumerate}
\item
  The user instructs the kernel to read from a file descriptor.
\item
  The calling process is put into a sleeping state, and it is inserted into a wait queue.
\item
  The kernel, through it's networking layer instructs a network interface, to read data.
\item
  The device finishes reading, copies data into the main memory, and notifies the kernel.
\item
  The kernel copies the data to the user's memory space, and notifies members of the wait queue for this event. The process is put into a runnable state, and once scheduled, it will run in user mode.
\end{enumerate}
\begin{figure}[h]
\centering
\includegraphics[scale=1]{figures/findCart}
\caption{
The findCart function implemented with non-blocking IO
}
\label{fig:find-cart}
\end{figure}
I've also described event loops based on readiness change notification. For a description
of the idea see section \ref{s:readiness-notification}. Figure \ref{fig:find-cart}
 shows the \ic{findCart} method of our example database (as seen in listing
\ref{lst:thread-example-database}) transformed to a non-blocking version using this concept.
\par
Compare this implementation with what the Linux kernel does. As a matter of fact,
these event loops can be thought of as userspace implementations of the kernel wait 
queues. The readiness notification handle is subscribed by the kernel to all of the
relevant wait queues. The notifications are then demultiplexed in the userspace
instead of the kernel, allowing us to bypass threads, and replace them with our
own task implementation. This mechanism is crucial for any alternative task model.
\begin{listing}
\caption{Nested callback interface for the database of the sample program}
\label{lst:nested-callback-example-database}
\begin{minted}[linenos]{Java}
interface Database {
    void findCart(String id, Consumer<Cart> onComplete);
    void decreaseItemStock(CartItem item, Runnable onComplete);
    void savePurchase(List<CartItem> items, Date ts, Runnable onComplete);
}
\end{minted}
\end{listing}
\begin{listing}
\caption{The example program implemented with nested callbacks}
\label{lst:nested-callback-example}
\begin{minted}[linenos]{Java}
Database db = new Database();
String cartId = request.cookies.get("sessionId");
db.findCart(cartId, cart -> {
    Latch latch = new Latch(cart.items.size(), () -> {
        db.savePurchase(cart.items, new Date(), () -> {});
    });
    for(Item item : cart.items) {
        db.decreaseItemStock(item, () -> latch.countDown());
    }
})

class Latch {
    private final int num;
    private final Runnable onComplete;

    Latch(int num, Runnable onComplete) {
        this.num = num;
        this.onComplete = onComplete;
    }

    public void countDown() {
        if(this.num-- <= 0) {
            onComplete.run();
        }
    }
}
\end{minted}
\end{listing}
\subsection{Programming model}
Converting all of the methods in the \ic{Database} interface gives us the version seen in
listing \ref{lst:nested-callback-example-database}.
Using this interface, our task can be implemented as shown in listing \ref{lst:nested-callback-example}.
Although I've argued that under the hood the two models are very similar, on the surface
they are surprisingly different. The multiple stages are clearly separated, as they
are in different event handlers of the program. In the blocking model the operations
were declared more abstractly, the programmer didn't have to think about how the
framework will invoke a callback on an event occurring.
\par
Instead of declaring steps one after an other, the programmer has to connect event
handlers to react to completion events. Some\cite{event-based-programming-without-ioc}
call this an inversion of control. Although control was never in userspace, with the
use of callbacks even the illusion is taken away.
\par
I've introduced a class named \ic{Latch} that is a non-blocking form of the 
\ic{java.util.concurrent.CountDownLatch}.
This latch is a synchronization tool that creates a rendezvous point, that lets the
program proceed only after a number of events have occurred. 
\par
Since \ic{decreaseItemStock} typically returns before the actual database
operation could be completed, the IO operations initiated by function calls
in the for loop will run concurrently, and we have to make sure that we only proceed
with the task once all the stocks are decreased. 
\par
\begin{listing}
\caption{
The example program implemented with nested callbacks, with \ic{decreaseItem}
calls serialized.
}
\label{lst:nested-callback-exact}
\begin{minted}[linenos]{Java}
Database db = new Database();
String cartId = request.cookies.get("sessionId");
db.findCart(cartId, cart -> {
    ListLatch<CartItem> l = new ListLatch<>(cart.items);
    l.doOnEach = item -> db.decreaseItemStock(item, () -> l::next);
    l.onComplete = () -> db.savePurchase(cart.items, new Date());
    l.next();
})

class ListLatch<T> {
    private int index = 0;
    private final List<T> elements;
    public Runnable onComplete;
    public Consumer<T> doOnEach;

    public ListLatch(List<T> elements) {
        this.elements = elements;
    }

    public void next() {
        if(index >= elements.size()) {
            onComplete.run();
        } else {
            doOnEach.accept(elements.get(index++));
        }
    }
}
\end{minted}
\end{listing}
This is a special case, as the items may be considered independent. If the blocking
model had to be recreated exactly, it might look similar to listing
\ref{lst:nested-callback-exact}. This illustrates that simple structures taken for
granted in the blocking model take some thought when instructions return before
their intended effects occur.
\par
In the example program the task dependencies have a depth of at most two, but deeper
dependencies for more complicated workflows may result in convoluted-looking,
nested functions. This is sometimes referred to as \textit{callback hell}, exhibiting
the general attitude towards this programming model.
Lambda expressions make the code more presentable, but before Java 8, code with 
nested callbacks was especially hard to read, as programmers usually declared
callbacks by instantiating anonymous inner classes.
\par
\begin{listing}
\caption{
Use of the try-catch block with nested callbacks does not work, as 
execution of the try block merely instantiates the functional interfaces
implemented by the lambda body, but does not execute them.
}
\label{lst:nested-callback-trycatch}
\begin{minted}[linenos]{Java}
try {
    Database db = new Database();
    String cartId = request.cookies.get("sessionId");
    db.findCart(cartId, cart -> {
        ListLatch<CartItem> l = new ListLatch<>(cart.items);
        l.doOnEach = item -> db.decreaseItemStock(item, () -> l::next);
        l.onComplete = () -> db.savePurchase(cart.items, new Date());
        l.next();
    });
} catch(Throwable t) {
    log.error("Purchase failed", t);
    redirectToErrorPage();
}
\end{minted}
\end{listing}
The nested callback approach complicates error handling. 
In my last example, I've omitted the try block, to keep the code more simple, 
but let's imagine I were to put it back as seen in listing
\ref{lst:nested-callback-trycatch}.
\par
The structure of the code in listing \ref{lst:nested-callback-trycatch} may suggest
that any \ic{Throwable} thrown while evaluating code in the try block will be
handled in the catch block, but lambda expressions introduce a subtle difference
between evaluating the lambda expression, and execution of the lambda body.
As the Java Language Specification\cite{java-language-specification} states:
\par
\textit{At run time, evaluation of a lambda expression is similar to evaluation of a class
instance creation expression, insofar as normal completion produces a reference
to an object. Evaluation of a lambda expression is distinct from execution of the
lambda body.}
\par
The try-catch block ensures that throwables thrown while executing statements 
inside the try block will transfer control to the catch block. 
In this case execution of the try block merely instantiates the functional interfaces
implemented by the lambda body, but does not execute them.
\subsubsection{State management}\label{s:nested-callback-state-management}
As I've mentioned previously, the continuous-looking program is divided into separate
event handlers, along state boundaries. The event handlers can be thought of as
miniature sub-tasks, connected indirectly. The top level function that registers
interest in the first event, and provides a callback returns immediately.
\par
This way the call stack becomes useless to track execution state, as functions 
are only connected logically, since they don't call each other. These connections
are not represented in the executing thread's stack, as the function declaring
interest for an event returns before the event handler is invoked by the IO handling
layer.
\begin{listing}
\caption{Stack trace of a program affected by stack ripping}
\label{lst:stacktrace-stack-ripping}
\begin{lstlisting}
"nioEventLoopGroup-3-1@8073" prio=10 tid=0x15 nid=NA runnable
    at hu.tegi.bookshop.pg.PgPurchaseBooksOperation.lambda$execute$6
    at com.github.pgasync.impl.PgConnection$1.onCompleted
    at com.github.pgasync.impl.netty.NettyPgProtocolStream$1.onCompleted
    at com.github.pgasync.impl.netty.NettyPgProtocolStream$5.channelRead
    at io.netty.channel.AbstractChannelHandlerContext.invokeChannelRead
    at io.netty.channel.AbstractChannelHandlerContext.fireChannelRead
    at io.netty.handler.codec.ByteToMessageDecoder.channelRead
    at io.netty.channel.AbstractChannelHandlerContext.invokeChannelRead
    at io.netty.channel.AbstractChannelHandlerContext.fireChannelRead
    at io.netty.handler.codec.ByteToMessageDecoder.channelRead
    at io.netty.channel.AbstractChannelHandlerContext.invokeChannelRead
    at io.netty.channel.AbstractChannelHandlerContext.fireChannelRead
    at io.netty.channel.DefaultChannelPipeline.fireChannelRead
    at io.netty.channel.nio.AbstractNioByteChannel$NioByteUnsafe.read
    at io.netty.channel.nio.NioEventLoop.processSelectedKey
    at io.netty.channel.nio.NioEventLoop.processSelectedKeysOptimized
    at io.netty.channel.nio.NioEventLoop.processSelectedKeys
    at io.netty.channel.nio.NioEventLoop.run
    at io.netty.util.concurrent.SingleThreadEventExecutor$2.run
    at io.netty.util.concurrent.DefaultThreadFactory$DefaultRunnableDecorator.run
    at java.lang.Thread.run
\end{lstlisting}
\end{listing}
This is called \textit{stack ripping}. The name illustrates the fact that the stack is
destroyed between execution stages. Imagine that an exception occurs at line 4 of listing
\ref{lst:nested-callback-exact}. The stack trace would show something similar to
listing \ref{lst:stacktrace-stack-ripping}. This would provide the information that some
event occurred in the network stack, handled by netty, passed to the postgres driver,
which invoked a lambda expression in my program. The example program is fairly trivial,
therefore it is not difficult to identify what happened, but if it was much more
complicated, or I wasn't familiar with the code, this stacktrace would provide
little to no useful information.
\par
Stack ripping also affects debugging tools. It is no longer possible to simply
step through the program line by line, and see operations and their effects, as
IO operations are merely registering intents. A breakpoint has to be put to the
event handler, which makes tracing execution more difficult.
\par
Although stack ripping has disadvantages, it has a minor beneficial effect on performance.
Discarding the stack between stages of the task results in the stack being considerably
more shallow, thereby occupying less space in memory. Stack overflows are rarely an issue.
Even if tasks are complicated, when they stop for IO, their stacks are essentially emptied.
The amount of memory saved by shallow stacks is not significant, as we have very
few threads.
\par
The nested callbacks method cannot make use of local variables to store partial results,
as a single function cannot cover multiple states dispersed by IO calls.
As I will show later, this problem is circumvented by lambda expressions, but
languages without comparable functionality present a great disadvantage for programs
with the nested callback style.
\par
Use of local variables can be avoided entirely. One approach is to pass partial results
as function arguments. As the number of partial results grow, the function signatures
would become less readable. If methods are kept short, and partial results are few,
this approach is applicable. Keeping methods short is a good practice at any rate,
as shorter methods are easier understood. This style is somewhat limited, as some
languages --- like Java --- do not allow function arguments to be passed by reference.
This prevents a chain of functions mutating arguments that they receive.
\par
Another approach to avoid using local variables is to store them as (non-static)
fields of a class.
This allows access to these variables for all methods of the declaring class.
Partial results don't have to be passed as function arguments, and their value
may be mutated across function calls. Instance fields might be thought of as
implicit function arguments, as the \ic{this} parameters is implicitly available
to all non-static functions of a class.
Some do not prefer this style, as use of implicit variables may be confusing
for someone who wants to understand a class. 
\par
I think that as long as a class is not too complicated, usage of
field variables to store partial results is justifiable.
The drawback of this approach, is that objects used in this fashion should not
be shared between tasks. If two tasks executing concurrently shared the same
instance, they would mutate each others execution state in an unpredictable fashion.
\par
In my opinion the most convenient approach to replace local variables is to use
a language construct that allows declaration of (anonymous) code which captures 
the state of variables of it's declaring scope. 
In Java there are at least two approaches for this: anonymous inner classes
and lambda expressions. These two methods are similar in effect, but lambda
expressions are a lot less verbose syntactically.
\par
There are a few ways to implement lambda methods as outlined by \cite{lambda-implementation}.
One of these is that the compiler generates a function from the lambda that receives
visible variables as arguments. This is basically syntactic sugar on top of
the approach that I've outlined earlier. Lambda expressions can only access effectively
final local variables, so just like the argument passing approach, lambdas cannot
modify these arguments\footnote{
Although this is due to the fact that lambda functions are defined this way.
They could be implemented in a way that would allow mutation of local variables,
but this is forbidden by the specification.
}.
\par
Anonymous inner classes may implement any class, but evaluation of lambda expressions 
can only  result in instances of functional interfaces. 
Callbacks can be represented as single method interfaces, so this approach should
be sufficient.
\subsubsection{Task context}\label{s:nested-callbacks-task-context}
Simply put the operating system thread is a structure that keeps track of the task's 
execution state. It is a tool to make a concrete connection between functions
that depend on each other's functionality. Threads are explicitly created, and pointed
to the first method they should execute. If this method is not capable of carrying
out the necessary task, it invokes another method. This other method gets pushed
onto the stack, and thus a connection is made.
\par
When a task needs to pause and wait, it's state is maintained in memory thanks to
the context switch carried out by the operating system. A scheduler takes care of
restarting the thread, and resuming it's execution state. This supporting system
allows methods participating in a task's execution to know little to nothing about
each other, or the structure of which they are a part. At the same time there is
a possibility of introspection, as the runtime allows querying the
thread in which the code is running.
\par
In the case of nested callbacks, there is no such system. Tasks are not created
implicitly, execution of their first part begins, when an event occurs.
Methods are only active until they would have to block, at which point they simply
register the next method that should execute when the waited event occurs, and
the original method completes execution, it is no longer active in any form.
Different stages of a task are connected only logically, otherwise they might
as well be separate tasks.
\par
Thread local storage is not reliable, as separate threads can --- and probably will ---
execute different parts of the task, and multiple tasks may be executed by a
single thread.
\par
For these reasons, the nested callbacks approach (in this simple form) doesn't support
a task context.
\subsection{Performance}
As I have shown in the previous sections, the nested callbacks approach uses the
bare minimum that is needed to support non-blocking IO.
\par
There is no task context, or supporting structures that only exist to facilitate
storing execution state of the task. This method takes what the operating system
does to implement blocking calls with threads, and strips away all that is not
necessary to create a system that links together parts of a task separated
by delayed IO operations. 
Memory usage is at a bare minimum, there is no minimal size for the task, only
necessary memory is allocated.
\par
Most importantly there is no scheduling algorithm, when a ``task switch'' occurs, 
the next task is selected by it's position in the wait queue of the event which 
is being handled. 
This can be done in a few processor instructions, and there are no cache 
effects of this ``scheduling'', other than the implicit effect of the different 
memory usage pattern that the next task exhibits.
\par
Tasks are run until they arrive at a point where they would have to wait, they
are not interrupted. 
If the nested callbacks approach would have a scheduling algorithm, I would
categorize it as cooperative, although tasks don't yield voluntarily, they yield
the CPU when they are finished.

\section{Composable computation}\label{s:composable-computation}
As I have shown in the previous section, the nested callbacks approach is a minimal
--- and therefore very efficient --- way to implement tasks with non-blocking IO.
A side effect of it's low performance is a somewhat basic programming model.
Deep nesting of callbacks is hard to follow, and correct error handling requires a
lot of careful thought. Composable computations try to address these shortcomings.
\par
In Java, a \ic{Future} is a class that represents the result of an asynchronous
computation. It has a rudimentary interface, which allows checking if the value
is ready, and waiting for the value to complete. Unfortunately this waiting blocks
the thread on which it was initiated, making it useless for non-blocking asynchrony.
\par
Java 8 introduced an interface called \ic{CompletionStage}, that represents an
asynchronous computation, and also allows registering callbacks to run when the
computation completes, thereby it allows waiting for the computation asynchronously.
\par
Another vital feature of the \ic{CompletionStage} that it is composable.
In this context composition means that a computation can be appended to the
\ic{CompletionStage}, which takes the future value, and transforms it into another
future value, thus producing another \ic{CompletionStage}.
Using this method chains of operations can be expressed on a future value.
This mechanism is perfectly sufficient to glue together stages of a task dispersed
by non-blocking IO.
The default implementation of the \ic{CompletionStage} is \ic{CompletableFuture},
which is a class that implements both \ic{Future} and \ic{CompletionStage}.
\subsection{Programming model}
\begin{listing}
\caption{Interface for the database of the sample program using \ic{CompletableFuture}s}
\label{lst:completable-future-example-database}
\begin{minted}[linenos]{Java}
interface Database {
    CompletableFuture<Cart> findCart(String id);
    CompletableFuture<?> decreaseItemStock(CartItem item);
    CompletableFuture<?> savePurchase(List<CartItem> items, Date ts);
}
\end{minted}
\end{listing}
\begin{listing}
\caption{The example program implemented with \ic{CompletableFuture}s}
\label{lst:completable-future-example}
\begin{minted}[linenos]{Java}
Database db = new Database();
String cartId = request.cookies["sessionId"]
db.findCart(cartId).thenCompose(cart -> {
    CompletableFuture<?> merged = CompletableFuture.completedFuture(null);
    for(Item item : cart.items) {
        merged = merged.thenCompose(r ->
            db.decreaseItemStock(item);
        );
    }
    return merged;
}).thenCompose(r2 -> 
    db.savePurchase(cart.items, new Date());
).exceptionally(t -> {
    log.error("Purchase failed", t);
    redirectToErrorPage();
    return null;
}).get();
\end{minted}
\end{listing}
The database interface has been changed, so that each method returns a 
\ic{CompletableFuture} as seen in listing \ref{lst:completable-future-example-database}.
Two out of three methods are computations without a return value. 
Since there is no separate type for a computation without value, they are represented by
\ic{CompletableFuture<?>}, returning \ic{null}.
\par
\ic{CompletableFutures} can be derived from methods accepting callbacks.
Listing \ref{lst:find-cart-mock-impl} shows a mock implementation of \ic{findCart}, 
that uses a non-blocking database driver, which uses callbacks to implement 
the \ic{CompletableFuture} interface.
\begin{listing}
\caption{A mock implementation of \ic{findCart}}
\label{lst:find-cart-mock-impl}
\begin{minted}[linenos]{Java}
CompletableFuture<Cart> findCart(String id) {
    CompletableFuture<Cart> future = new CompletableFuture<>();
    driver.executeQuery(
        "select * from cart where id = " + id, 
        resultSet -> { 
            future.complete(mapResultSet(resultSet));
        }
    );
    return future;
}
\end{minted}
\end{listing}
\par
Taking a look at the \ic{CompletableFuture} implementation of the example program,
it is obvious, that of the many methods that \ic{CompletionStage} provides, 
\ic{thenCompose} is the most crucial one.
It takes a function, with signature \ic{Function<? super T, ? extends CompletionStage<U>>},
where \ic{T} is the type of the result of the \ic{CompletionStage} that 
\ic{thenCompose} is applied to. When the computation reaches the current stage,
this function is applied to the current result, which transforms it to a future result
of type \ic{U}.
\par
Instead of nesting callbacks, \ic{thenCompose} allows the programmer to chain method
calls, creating a pipeline of operations. This eliminates the ``callback hell'' problem. 
The composable computation approach has the drawback that there can only be
a single result, while callbacks can accept multiple arguments.
\par
\begin{figure}[h]
\centering
\includegraphics[scale=1]{figures/completableFuture}
\caption{
The \ic{CompletableFuture} sets up a pipeline of transformations
using \ic{thenCompose}
}
\end{figure}
Error handling is much more elegant. Methods implementing stages of tasks do not
register each other directly, instead the \ic{CompletableFuture} takes the result
when it's available, and invokes the transformation function of the next stage.
This allows the \ic{CompletableFuture} to control error handling.
If any of the \ic{CompletionStage}es fail to complete normally, the pipeline
finishes exceptionally, and the control is transferred to the error handling logic
defined as part of the pipeline via the \ic{exceptionally} method.
\par
Although error handling has improved, there is still a caveat.
If care is not taken to handle errors, and complete the \ic{CompletableFuture}
exceptionally, the pipeline could get stuck, as the current completion is not
completed at all.
\par
\begin{listing}
\caption{
A mock implementation of \ic{findCart}, with errors handled correctly.
In this example the \ic{executeQuery} takes an optional third argument, that
is a callback which is fied when an error occurs while executing the query.
}
\label{lst:find-cart-mock-impl-error-handling}
\begin{minted}[linenos]{Java}
CompletableFuture<Cart> findCart(String id) {
    CompletableFuture<Cart> future = new CompletableFuture<>();
    driver.executeQuery(
        "select * from cart where id = " + id, 
        resultSet -> { 
            future.complete(mapResultSet(resultSet));
        },
        error -> {
        	future.completeExceptionally(error);
        }
    );
    return future;
}
\end{minted}
\end{listing}
Take listing \ref{lst:find-cart-mock-impl} as an example.
If the network was unreachable, the \ic{executeQuery} method would throw an
exception, and as that exception is not handled in any way, it would block any
future computations depending on this stage. A correct implementation is shown in
listing \ref{lst:find-cart-mock-impl-error-handling}.
\subsubsection{State management}
Execution state is stored in the \ic{CompletableFuture} object, as it stores the
whole computation as a chain of transformations.
This pipeline can be stored on the heap as an ordered memory structure like a 
linked list, expanding dynamically, thus not occupying memory that is not used.
\par
Similar to the nested callbacks approach, \ic{CompletableFuture}s are affected
by stack ripping, so the same strategies can be employed that I described in
section \ref{s:nested-callback-state-management} to replace local variables for
partial result storage.
\subsubsection{Task context}\label{s:composable-computation-task-context}
Thread local storage is unreliable for the same reasons that I've described in section
\ref{s:nested-callbacks-task-context}.
\par
Stages of the task are now related through the pipeline they belong to, and 
the pipeline runs all code that is part of the task, but implementations that I've
used do not provide means to query the current pipeline.
\subsection{Performance}
The composable computation approach builds on callbacks, but trades some overheads
to store the whole operation instead of only logically connecting them for more
sophisticated error handling, and composition.
For these reasons the performance should be very similar, with the overhead of the
pipeline storage (increased memory usage and garbage collector pressure).
\par
\ic{CompletableFuture}s are not scheduled by a scheduler, tasks run until they
would have to block, or they complete.
Selecting the next task to run is simply looking up the \ic{Future}s that depend
on the event that occurred. A minimal algorithm has to run on task switch, as
the pipeline has to be maintained at each stage.

\subsection{RxJava}\label{s:rx-java}
RxJava\cite{rxjava-github} is a Java VM implementation of Reactive Extension, which 
is a library for composing asynchronous and event-based programs by using observable 
sequences. This library implements the composable computation model, but offers
much more functionality then what is requreid for the semantics of this approach.
\par
Observables are streams of data, that an observer can observe. Observers are notified
on each piece of data (\ic{onNext}), the end of the stream (\ic{onCompleted}) and
errors (\ic{onError}). RxJava offers operators that transform, or combine these
observables. Using these operators the user can establish a pipeline similar to
the one shown in section \ref{s:composable-computation}.
The future result of a computation is a stream of a single piece of data.
Using this analogy, it is obvious that RxJava is a superset of the composable 
computation model.
\par
\begin{figure}[h]
\centering
\includegraphics[scale=0.45]{figures/flatMap}
\caption{
The \ic{flatMap} operator provides similar functionality to \ic{thenCompose}.
Image source: http://reactivex.io
}
\end{figure}
The functionality provided by \ic{CompletableFuture}'s \ic{thenComponse} is 
supplied by the \ic{flatMap} operator in RxJava. This operator takes a function
which transforms elements emitted by the source to another Observable.
These transformed emissions are then merged via the \ic{merge} operator, thereby
resulting in a single Observable.
This is similar to \ic{thenComponse}, but instead of being only applicable to
one element, it can be used on a stream (or list) of elements. The special case
where there is only one emission from the source Observable, this functionality
is equivalent to \ic{thenCompose}.

\par
RxJava has a abundance of predefined operators, but what is even more useful is
that it provides the user to define new operators. I will show such an operator in
section \ref{s:example-web-layer}.

\section{Fibers}
Fibers are structures that represent the execution of some code. This execution
can be suspended, and later resumed from where it was stopped.
This description would fit the operating system threads, but fibers are handled
entirely at the user level. Fibers are sometimes referred to as userspace threads,
or green threads.
\par
Since the user can create, suspend and resume a fiber, it is possible to use a
application specific scheduling algorithm while maintaining the familiar programming
model of threads.
\par
Fibers are multiplexed onto operating system threads, since all user level programs
run in processes. Many fibers belong to an operating system thread, and there are
a number of threads equal to the logical CPU cores available to the system.
If a fiber calls a method that blocks the underlying operating system thread,
all threads belonging to the thread get blocked.
\par
To avoid blocking threads, programs using fibers need fiber blocking operations
instead of thread blocking operations.

\begin{figure}[h]
\centering
\includegraphics[scale=0.75]{figures/fibers}
\caption{
Fibers are scheduled using operating system threads 
}
\end{figure}

\subsection{Programming model}
To illustrate the programming model of fibers, I am going to use the Quasar 
\cite{quasar-documentation} library for the JVM.
Note that this is not the only possible implementation, and features and performance
of fibers may vary based on the implementation details.
\par
\begin{listing}
\caption{The example program implemented with Quasar fibers}
\label{lst:fiber-example}
\begin{minted}[linenos]{Java}
try {
    String cartId = request.cookies.get("sessionId");
    Cart cart = db.findCart(cartId);
    for(Item item : cart.items) {
        db.decreaseItemStock(item);
    }
    db.savePurchase(cart.items, Date.current);
} catch(Throwable t) {
    log.error("Purchase failed", t);
    redirectToErrorPage();
}
\end{minted}
\end{listing}
\begin{listing}
\caption{The database interface for the fiber implementation}
\label{lst:fiber-example-database}
\begin{minted}[linenos]{Java}
interface Database {
    Cart findCart(String id) throws SuspendExecution;
    void decreaseItemStock(CartItem item) throws SuspendExecution;
    void savePurchase(List<CartItem> items, Date ts) throws SuspendExecution;
}
\end{minted}
\end{listing}
As it's easy to see in listing \ref{lst:fiber-example}, the program looks exactly
the same as the one using threads. This is to be expected by a model that is
a user level thread. What we cannot see in this short piece of code, that this
is of course running in the context of a fiber instead of a thread.
\par
Examining listing \ref{lst:fiber-example-database}, we see the only difference to the
thread model. All of our database methods are declaring to throw a
\ic{SuspendExcution} exception. This declaration is needed for the implementation
of Quasar fibers, as I will briefly describe in the next section.
\par
The problem with fibers that they need libraries that block fibers, but not threads
to work as expected.
Since there is no single standard implementation of fibers on the JVM, a fiber
library needs to be able to convert other libraries to fiber blocking libraries.
\par
Quasar provides the \ic{FiberAsync} helper class to transform asynchronous methods 
into fiber-blocking methods. This class connects the success and error callbacks
to the logic that handles resuming the fiber.
Providing fiber blocking libraries using this method is a smaller effort, and
library writers don't need to know about fiber management.

\subsubsection{State management}\label{s:fiber-state-management}
As the goal of fibers is to imitate threads, I am not going to describe the stack
and local variables as state storage, as I've already described them in section
\ref{s:thread-state-management}.
\par
The difference between threads and fibers lie in the implementation details.
I have already described threads in an earlier section, I will briefly show
how Quasar implements fibers.
\par
The Quasar library uses bytecode instrumentation to be able to suspend the
execution of a fiber. 
The bytecode is examined, and each call to a suspendable method
--- i.e. a method that declares the \ic{SuspendExecution} exception --- 
is surrounded by code that saves and restores the fiber's execution context.
\par
If the execution of the suspendable method really blocks the fiber, by calling
the \ic{Fiber.park} method, the \ic{SuspendExecution} will be thrown, and
handled by the fiber system. When the suspendable function returns, the calling
function's context is restored by the instrumentation code.
\par
The Quasar library depends on bytecode rewriting, which means that compiled
code needs another pass by the bytecode rewriting library to work as expected, 
but this is necessary due to the fact that the JVM doesn't enable manipulation 
of the stack directly.

\subsubsection{Task context}
A thread can only run one fiber at a time, and the scheduler selects the fiber
to run on each thread, therefore the scheduler can easily set a thread local
global variable with the value of the current fiber's id.
As I have already shown, this is sufficient to establish a task-local context.

\subsection{Performance}
It's difficult to make general statements about fibers, as many implementations
exist. The important aspect is that fibers are not constrained by the restrictions
of having to implement a generic task system.
For example a fiber system may use a simpler, cooperative
scheduler, because it can act on the information that a typical task is not CPU
intensive, and will yield the processor often.
\par
Fibers are usually expected to follow the same programming model as threads,
but distinct implementations can use different methods to achieve this model.
A fiber system may use OS thread stacks, or it may rewrite the code to use state
machines on the heap to manage the program state.
