%----------------------------------------------------------------------------
\chapter{Introduction}

\section{Motivation and goals}
The widespread adoption of internet services has changed expectations of how the systems implementing them should work. For example Google's search engine serves approximately 3.5 billion searches a day \cite{internetlivestats}. It is a challenge to build an application that can serve this traffic while trying to minimize the costs of such a system. A strategy for keeping costs low is using commodity or virtualized hardware, even in an operation of Google's scale \cite{google-platform}. It's not just giant projects that need to be economical, I would argue that every application should minimize it's resource consumption within the boundaries of it's requirements. The main question of this thesis is what programming approaches facilitate such resource-conscious solutions under high degrees of concurrency. In particular my interest are applications that need to access relational databases.
\par
One of the recently popular technologies is asynchronous processing. It aims to reduce costs under certain workloads by lowering the number of threads used in an application. Asynchronous processing is not a new idea, but many of today's mainstream programming languages - such as Java - are struggling to quickly implement standard libraries that enable developers to write programs in an asynchronous fashion. One of the areas that are clearly lagging is asynchronous interfaces to communicate with relational database management systems (RDBMS), especially in Java. There are only a couple of projects targeting this issue, and only a few of them are actively maintained. It is not surprising that there is no standard for asynchronous RDBMS access, such as the standardized synchronous relational database interface, the Java Database Connectivity(JDBC).
\par
Grails \cite{grails-homepage} is a Groovy-based web application framework. The recent major version - Grails 3 - introduced profiles. Profiles are a way to describe which subset of the many features provided by Grails a particular application wishes to use. According to the project's roadmap \cite{grails-roadmap}, one of the future profiles aims to utilize asynchronous processing. This is not possible without all of the used libraries being asynchronous. One of the key features of Grails is easy data manipulation, so an asynchronous RDBMS driver is clearly necessary. There are non-relational targets as well, but RDBMS's have been, and - in my opinion - will be a very popular choice.

\section{The structure of this thesis}
In Chapter \ref{c:io-models} I will take a look at how blocking and non-blocking 
input and output methods work, as this is the foundation for asynchronous processing. 
\par
Chapter \ref{c:threads} will explain in detail what features threads provide, and
how these features might be provided by an operating system.
This explanation will show how using fewer threads can --- under 
certain circumstances --- reduce the resource usage of an application. 
\par
Using threads has been the de facto way of dealing with concurrency for a long time, 
but utilizing an approach tailored for a certain workload may yield better performance. 
Chapter \ref{c:alternative-task} takes a look at some of these alternative approaches, 
and how they can be used to handle concurrent tasks, and how they compare with threads.
\par
After investigating different models, I choose one and create an application
in Chapter \ref{c:example-application} to
gain knowledge of differences in the programming model compared to threads.
\par
I conclude the thesis with performance benchmarks in Chapter \ref{c:performance},
which illustrates the special circumstances in which the non-blocking model thrives.

\section{Choices of technology}
It is difficult to describe topics very broadly, in a way that is applicable to every scenario. One of the topics that is very hard to reason about in general is the field of operating systems. I chose Linux (kernel version 3.16.0-33) where I need to describe OS specific details. I feel like this is a good choice, as it is relatively easy to study, and also popular in server deployments. Sections about Java are written with the JDK version 8 in mind, using the HotSpot virtual machine.

\section{Glossary}
Some terms I use are very overloaded, and therefore they might be ambiguous. Below I list the preferred definitions for such terms to reduce ambiguity.
\par
\begin{itemize}
	\item \textbf{Process}: An operating system process.
	\item \textbf{Thread}: A unit of execution that is blocked by a blocking system call. More specifically a Linux lightweight process.
	\item \textbf{Task}: A piece of work to be done. A high level of abstraction, usually described in a declarative manner, or as a sequence of steps. When described sequentially, it is similar to how a thread executes logically.
	\item \textbf{Asynchronous}: Two tasks are synchronous if one must finish before the other can continue. Asynchronous is the opposite of this.
\end{itemize}
%----------------------------------------------------------------------------
