\chapter{An example application}\label{c:example-application}
To put the ideas I have discussed in previous chapters into practice, I wanted
to create an application that solves some common problems.
Through this application I was hoping to better understand the difficulties that 
arise while implementing an application using non-blocking IO.

\section{Specification}

\subsection{Technical requirements}
I usually think that functional --- or business --- requirements should precede
technical requirements when planning a software project, but in this case I reversed
the process. I wanted to make an application that fits into the programming
ecosystem that I am targeting, which is the JVM, and more specifically Grails,
with a relational database as data storage.

\subsection{Functional requirements}
Grails is often used for rapid\footnote{
Of course by stating this, I don't want to imply that Grails is not 
applicable for large projects
} web application development, so it was obvious that I would be developing
a customer facing web application that runs in the browser.
\par
The other technical constraint --- the relational database --- is very general purpose,
but I think it is very useful in scenarios where transactions, sophisticated queries
and rich data structures are required. I've selected an online retail as a business
domain that exhibits these properties. To further specify, and therefore simplify
the problem, I decided to create a web application that enables a publisher to
sell their books.
\par
It was not my goal to create an application that I would ship to a customer,
I only wanted to focus on the parts that concerned database access, and non-blocking
input and output methods. For these reasons I've explicitly omitted crucial parts of
an online store such as security, user experience, and financial functions.
The domain logic is very simple, to allow more time for discussion of technical
decisions.

\subsubsection{Domain objects}
\begin{itemize}
\item A \textbf{Book} is a publication that may be purchased.
\item The \textbf{Catalog} is a set of books that the store is currently selling.
\item A \textbf{Customer} is a visitor of the store.
\item The \textbf{Cart} is a set of items that a customer has collected for purchase.
\item A \textbf{Manager} is someone who maintains the store, especially the contents of the catalog.
\end{itemize}

\subsubsection{Stories}
\begin{itemize}
\item A manager can add, remove and modify books of the catalog.
\item A customer can list all items in the catalog.
\item A customer can put an item in their cart. If the item was not already in
the cart, it gets added with a quantity of one. If the item was already in the cart,
the quantity is increased by one.
\item A customer can empty the contents of their cart.
\item A customer may purchase the contents of their cart, by providing shipping
and payment information
\end{itemize}

\begin{figure}[h]
\centering
\includegraphics[scale=0.3]{figures/example-application}
\caption{
Checkout process of the example application
}
\end{figure}

\section{Concurrency}
As I have discussed in the previous chapters, the goal of this thesis is to examine
approaches to handling highly concurrent workloads, by using methods other than
operating system threads. 
\par
Of the approaches discussed in chapter \ref{c:alternative-task} I've chosen the
composable computation model. I perceived the nested callbacks approach as one with
a difficult programming model. I did not have a logical reason to not use fibers,
I simply felt like the composable computation approach was a more popular practice
ont the JVM, so I thought experience with this implementation may be more useful.
More specifically, I've chosen RxJava, which I introduced in section \ref{s:rx-java}.

\section{Accessing the database}
Grails offers GORM\cite{gorm-documentation} as a persistence interface.
Unfortuanetly --- as of version 5 --- GORM does not define an asynchronous interface.
The next interface I would consider as a Grails developer would be JDBC.
JDBC has the very same problem, it is a standardized interface, but does not consider
asynchronous execution.
\par
Unfortunately version 8 of the Java platform doesn't have a standard
asynchronous relational database access interface, and as far as I am aware there is
not even a de facto standard.
\par
Without any kind of a standard, applications are left to choose their own interfaces.
I ended up using an interface almost identical to the postgres-async-driver 
project\cite{alaisi-github}, as it fit my application's needs. This interface
consists of a few simple parts:
\par
\begin{itemize}
\item The \ic{QueryExecutor} is the central object. It can execute simple string
queries, and queries with bind values using prepared statements.
Execution results are retrieved as an observable of either a single \ic{ResultSet}
object, or multiple \ic{Row}s. The \ic{QueryExecutor} does not define in any way
how the query is executed, or how the connection is established with the database.

\item A \ic{Transaction} is a \ic{QueryExecutor} that executes all queries as part
of a database transaction. The transaction may be committed or rolled back.
If an error occurs while executing a query, the transaction is rolled back automatically.

\item A \ic{Connection} is a \ic{QueryExecutor} that maintains a single
connection to the database, and executes all queries submitted to it on this same
connection. A connection has to be closed manually. A connection can begin to
execute a transaction.

\item A \ic{ConnectionPool} is a \ic{QueryExecutor} that maintains multiple connections
with the database, and manages them in a pool, so that a connection doesn't have
to be built each time a query has to be executed.
\end{itemize}

\begin{figure}[h]
\centering
\includegraphics[scale=0.7]{figures/dbInterface}
\caption{
Class diagram for the database access of the example application
}
\end{figure}

These few interfaces met my query execution needs, but they do not solve the problem
of writing these queries. SQL is a rich language that is able to express complex
queries addressed to the database. As databases and client software communicate
through sockets, these complex structures have to be serialized in some form.
The standard way to do this is using strings, resembling the english language.
\par
Sometimes queries are simple, and constant in their structure. Other times queries
have to be constructed dynamically. A regularly used approach is concatenating
strings following the conditions that necessitate dynamic query building.
This approach is often hard to read, and hard to get right, as the Java compiler
cannot check if the string being built represents a valid SQL structure or not.
\par
JOOQ's\cite{jooq-site} query building module represents much of the SQL language
with Java objects and method calls, thereby creating a domain specific language
hosted by the Java language. JOOQ's reverse engineering module can create Java objects
that correspond to a relational database's objects, such as tables. 
As the DSL is written in Java, the compiler can check whether the expression written
using the DSL is a valid Java expression. The structure of the DSL ensures that
only expressions that map to valid SQL can be created using JOOQ.
\par
JOOQ can also execute the queries written in the DSL, and fetch their results,
but unfortunately it is coupled with JDBC, and therefore not applicable for my use case.
The idea however, that queries should be executed, and not strings be given to
query executors for execution is a sound object oriented principle, and I thought
I would like to adopt it.
\par
I've created the \ic{AsyncQuery} interface that represents an SQL query that can
be executed asynchronously, hence it's only method is \ic{execute}, returning
an \ic{Observable<ResultSet>}. 
The \ic{ResultSet} contains all \ic{Row}s returned by the query, and the \ic{Field}s
in these rows. I use JOOQ's \ic{Field} to access values of the rows.
\par
The abstract implementation of the \ic{AsyncQuery} interface accepts a \ic{DSL}
object, and a \ic{QueryExecutor}, and is capable of executing the query.
However the class is abstract, therefore subclasses have the responsibility
of defining a \ic{Query} using JOOQ's DSL.

\par
So far I've mainly discussed the structure of the database access: through what
interfaces objects should communicate with each other. Obviously these interfaces
must be implemented to make a working application. I've chosen PostgreSQL as my
target database engine for this application, as it is one of the major database
engines today, and it is a fairly well documented, open source application.
\par
In my zeal I started to implement the PostgreSQL wire protocol manually, but I
thought better, and sought out projects that had already made some progress in
this field. I have found a Scala based driver\cite{mauricio-github} that seemed
to have some activity. I discarded this project, as technically Scala libraries can be invoked
from Java code, but I choose not to include the supporting libraries necessary
for this approach.
\par
The other library I found\cite{alaisi-github} is written in Java 8, but has a bit
less activity and developers. This project also uses RxJava as it's concurrency
handling mechanism. I decided to use this project as my database driver, as it can
be used to easily implement the interfaces I've outlined.
Both these projects use Netty\cite{netty-site} to coordinate the TCP communication
used by the Postgres wire protocol.

\section{Transactions}
Grails encourages developers to put their business logic into services, that are
separated from the request processing part of the application.
This is the Model part of the Model View Controller architecture.
This might lead to the following thinking, when implementing cart checkout:
\par
\begin{itemize}
\item A user navigates to the checkout page
\item The user presses the checkout button. This sends an HTTP request to a URL,
which will be intercepted by the \ic{CartController}
\item The controller extracts parameters from the HTTP request (the cart id in this example), 
and passes it to the \ic{CartService}
\item The service modifies the database accordingly, and passes control back to
the controller.
\item A view is rendered to notify the customer of the successful purchase.
\end{itemize}
\begin{listing}
\caption{A cart service in the common MVC style}
\label{lst:cart-service}
\begin{minted}[linenos]{Java}
class CartService {
    @Autowired
    DSLContext dsl

    @Transactional
    void purchaseContents(String cartId) {
        decreaseStockQuery(cartId).execute();
        emptyCartQuery(cartId).execute();
    }

    Query decreaseStockQuery(String cartId) {...}
    Query emptyCartQuery(String cartId) {...}
}
\end{minted}
\end{listing}
An implementation of the service might look similar to listing \ref{lst:cart-service}.
Although in Grails, it is implicit, I've explicitly marked that the
\ic{DSLContext}\footnote{
The DSLContext is a JOOQ object that is able to create queries, and execute them
via JDBC.
} is injected by Spring. This means that all requests are using the same object.
\par
The \ic{purchaseContents} method is marked with \ic{@Transactional}, which roughly means
that before the method a transaction is going to be started, and when the method returns
successfully, the transaction will be committed.
This is implemented by providing the \ic{DSLContext} with a transaction aware data source,
which is managed by the spring transaction manager.
\par
The transaction manager is capable of starting a transaction, and \textit{binding it
to the current thread}, via means of a \ic{ThreadLocal}. This way both times the 
\ic{DSLContext} asks for a connection to the database, it will receive the same
connection belonging to the same database transaction. If a thousand users were
purchasing the content of their carts, it would be handled by the same service object,
but since the connection is local to the thread, they do not interfere with 
each other's progress.
\par
As I've described in section \ref{s:composable-computation-task-context}, composable
computation approaches do not have a task local context, which means that the same
mechanism cannot be used. I can imagine an implementation of the composable computation
model which maintains a task context, but I think an alternative transaction handling
approach is preferable, instead of trying to extend the composable computation model.
\par
\begin{listing}
\caption{Alternative approach to the cart implementation}
\label{lst:cart-service-alternative}
\begin{minted}[linenos]{Java}
class Cart {
    PooledDatabaseContext databaseContext;
    String id;
    
    Observable<Void> purchaseContents() {
        return databaseContext.startTransaction().flatMap(tCtx ->
                new DecreaseStockForCartItemsQuery(tCtx, id)
                        .executeQuery()
                        .flatMap(decr ->
                                new EmptyCartQuery(databaseContext, id)
                                        .executeQuery()
                                        .flatMap(__ -> tCtx.commit())
                                        .flatMap(__ -> Observable.empty())
                        )
        );
    }
}
\end{minted}
\end{listing}
The alternative approach is shown in listing \ref{lst:cart-service-alternative}.
In this approach, objects that belong to a certain task are only referenced until
the task is completed. The only object that is shared by tasks is the pooled database
context, which dispenses a new transaction for each \ic{startTransaction} request,
and ensures that tasks cannot receive each others transactional database context
(\ic{tCtx}). The code in this listing is more verbose than the implementation of
the previous approach. This is mainly due to the fact that the transaction is
started and committed manually, instead of an aspect oriented programming concept.

\section{Web layer}\label{s:example-web-layer}
The web layer of Grails is built on top of the servlet API. Before the 3.0 version
of the servlet API there was no means of handling HTTP requests asynchronously.
A thread was allocated at the start of processing, and returned to the thread pool
when the request was finished. Version 3 defined a new async request handling
model. 
\par
A request can be put into an async state, which signals to the servlet
container that the request should be kept around after the servlet returns from
the service method. This way the request handling thread can be returned to the
pool to handle other requests. The servlet that put the request into an async
state receives a context which it can use to run code in the original context
of the request.
\par
Grails is built on top of the Spring MVC framework, which uses ThreadLocal variables
as task-global variables as I've described in section \ref{s:task-context}.
This would be problematic to applications wishing to interact with the web layer
from callbacks. Grails solves this problem by it's own implementation of the
async servlet context. When a request is made to the context to run code in
the original servlet request's context, the ThreadLocal is restored to it's original
state when the handling was suspended.
A request handled asynchronously with Grails is shown in listing \ref{lst:catalog-index}.
\begin{listing}
\caption{A request handled asynchronously with Grails}
\label{lst:catalog-index}
\begin{minted}[linenos]{Java}
def index() {
    AsyncContext ctx = startAsync()
    catalog.findBooks().find()
            .map({ Book book -> book.details() }).toList()
            .flatMap({ List<BookDetails> books ->
                ctx.start({
                    render(view: "/catalog/index", model: [books: books])
                    ctx.dispatch()
                })
            }).subscribe();
}
\end{minted}
\end{listing}

The request has to be put into async mode before the request handling method would
return. To retrieve a Grails specific async servlet context I use the \ic{startAsync}
method. When the callback is received in the callback, I use \ic{ctx.start()}, and
pass it a runnable, which will be ran in the context of the original request.
I render the view, and call \ic{ctx.dispatch()} to dispatch the request to later
stages of the request handling pipeline.
\par
\begin{listing}
\caption{The onNext operator, which simplifies request handling with RxJava and Grails}
\label{lst:grails-rx-onnext}
\begin{minted}[linenos]{Java}
static <T> Observable.Operator<T, T> onNext(AsyncContext ctx, 
                                            Consumer<T> consumer) {
    Observer<T> observer = new Observer<T>() {
        public final void onCompleted() {

        }
        public final void onError(Throwable e) {
            redirectToErrorPage();
        }
        public final void onNext(T args) {
            ctx.start(() -> {
                consumer.accept(args);
                ctx.dispatch();
            });
        }
    };
    return new OperatorDoOnEach<T>(observer);
}
\end{minted}
\end{listing}
\begin{listing}
\caption{The onCompleted operator, which simplifies request handling with RxJava and Grails}
\label{lst:grails-rx-oncompleted}
\begin{minted}[linenos]{Java}
public static <T> Observable.Operator<Object, ?> onCompleted(AsyncContext ctx, 
                                                             Runnable runnable) {
    Observer<Object> observer = new Observer<Object>() {
        public final void onCompleted() {
            ctx.start(() -> {
                runnable.run();
                ctx.dispatch();
            });
        }
        public final void onError(Throwable e) {
            redirectToErrorPage();
        }
        public final void onNext(Object args) {
        }
    };
    return new OperatorDoOnEach<Object>(observer);
}
\end{minted}
\end{listing}

This is somewhat verbose, but fortunately this verbosity can be reduced by Rx's
operators. An Rx operator allows users of the library to reuse functionality
that is commonly used when transforming observables.
I've created the \ic{GrailsRx} class that defines two operators that encapsulate
how a result from an observable can be transformed to an action that is executed
in the original request context. This class is shown in listings 
\ref{lst:grails-rx-onnext} and \ref{lst:grails-rx-oncompleted}.
\par
GrailsRx's \ic{onNext} handles the case when a result is expected of the Observable
chain, while \ic{onCompleted} handles the case when no value is expected.
It encapsulates the redundant logic that is used in every Grails async request.
This operator can be used as seen in listing \ref{lst:catalog-index-grails-rx}.
\begin{listing}
\caption{An async request handled by Grails with the GrailsRx operators}
\label{lst:catalog-index-grails-rx}
\begin{minted}[linenos]{Java}
def index() {
    catalog.findBooks().find().map({ Book book -> book.details() })
            .toList()
            .lift(GrailsRx.onNext(startAsync(), {List books ->
                render(view: "/catalog/index", model: [books: books])
            }))
            .subscribe();
}
\end{minted}
\end{listing}

\section{Testing and object orientation}
\begin{figure}[h]
\centering
\includegraphics[scale=0.45]{figures/integration-tests}
\caption{
Passing tests do not verify that the software is working correctly,
but they may catch some errors
}
\end{figure}
During the development of the webshop application I wanted to gain more understanding
of test driven development. I've tried to follow the principles outlined in
\cite{growing-object-oriented} and implement them in this example program.
My experiences have shown me that test driven development is not as much a quality
assurance measure, as it is an aid that helps thinking about the design of a software project.
\par
After clarifying the initial scope of the application, for each user story I wrote
a test from the perspective of the user. Of course these tests have to run automatically,
so I've used Geb\cite{geb-page} as a browser automation tool to simulate user interaction.
Using Geb I was able to organize my test logic in reusable modules.
Once these tests were failing as they were supposed to be failing, I've started
to implement the application that would pass these tests.
\par
For each feature I've identified the relevant domain entities that I've represented
with interfaces. These interfaces are supposed to map the application domain as
close as possible. After identifying the objects, I've started to implement them.
Just like the whole user story, the implementation of an object started with a
failing test. This test did not exercise the whole application, but only a few
components that were necessary for the implementation of the interface.
After I was satisfied with the way the tests were failing, I've finally started
to implement the concrete classes that would implement the object that would
pass the test.
\par
My initial approach might not have been perfect, and I would need to review
my thinking, and refactor the code. These tests helped ensure that components
still followed the design decisions after the rewrite. Tests also helped catch
trivial errors that were syntactically correct --- therefore not caught by the
compiler --- but semantically flawed. An example is setting up the observable
pipeline in the controller, but forgetting to subscribe to it. This way when the
end to end tests ran, the request timed out, and my test was failing.