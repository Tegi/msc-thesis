\chapter{Benchmarks}\label{c:performance}
I conducted performance benchmarks to confirm the theoretical considerations
that I presented in the previous chapters. I've used two personal computers for the
benchmarks. The server was a Core i7-4710HQ CPU and 16 GB RAM. I've simulated clients
using a computer with Core i5-6600 and 16 GB RAM. I've used Ubuntu 14.04 Linux with
kernel version 3.19.0-42 and Java HotSpot version 1.8.0 b65. For the Grails tests
I've used Tomcat 8 with non-blocking connectors, \ic{maxConnections} set to -1, 
\ic{maxThreads} set to 8 for the non-blocking tests, and 50000 for the blocking tests.
\par
To compare the blocking and non-blocking model I set up a server serving a set
number of concurrent requests, and considered two measurements: 
requests/second (throughput) and resource consumption (memory and CPU).
To simulate clients I used wrk\cite{wrk-github} version 4.0.2.
\par
To allow the server to fork a high number of processes I had to set up the kernel
to disregard normal limitations. I also set up
the client machine with an extended range of TCP ports, improved TCP connection
reuse and an increased limit for file descriptors.
The configurations changes
are enumerated in listing \ref{lst:benchmark-kernel-setup}.
\begin{listing}
\caption{Setup of the Linux kernel for benchmarking}
\label{lst:benchmark-kernel-setup}
\begin{minted}[linenos]{Java}
/etc/security/limits.conf
* soft nofile 100000
* hard nofile 100000
* soft nproc 100000
* hard nproc 100000

/etc/sysctl.conf
net.ipv4.ip_local_port_range = 1025 61000
net.ipv4.tcp_tw_recycle = 1
net.ipv4.tcp_tw_reuse = 1
\end{minted}
\end{listing}
\par
First I wanted to test the example application I created in chapter \ref{c:example-application}.
I've set up wrk to maintain 1000 connections, using 4 threads and run this test
for 30 seconds: \ic{wrk -c 1000 -t 4 -d 30 s http://127.0.0.1:8080}. I ran this
test once without measuring, to warm up the JVM, and then measured.
This simulated 1000 clients requesting the catalog page, which served the list of
books from the database.
\par
The first test yielded about 3000 requests per second. This turned out to be
a bottleneck of the GSP rendering engine. The server machine
was not capable of rendering the catalog page with only 5 books more than 3000
times a second, even without database access, because at this point all 8 cores
of the server's CPU was utilized at 100\%. 
Since this was not a good measurement of the blocking and non-blocking
model, I've focused on other tests.
\par
My second test was simply rendering \ic{OK} as a text response instead of a 
GSP. The server machine was capable of rendering this response at a rate of
45000 requests/second, without any database access, so I've used this as a baseline
of the maximal capacity for Grails benchmarks during this phase of testing, as
the all 8 CPUs of the server were maximally saturated at this request/second count.
\par
The catalog page always accesses the database, so the number of requests that
the database can serve a second must be higher than the HTTP server, otherwise
benchmarking the catalog page would be constrained by the capacity of the database.
I've managed to increase the PostgreSQL database maximum connection count to
1000 concurrent connections, but past this point I've experienced diminishing 
performance \footnote{The official PostgreSQL documentation\cite{postgres-maxconnection}
doesn't recommend more than a few hundred connections}.
The database was able to serve 26000 queries to the catalog page (measured using
JDBC and JOOQ) using this setup.
\par
I am confident that experienced database administrators can increase the database
performance much further than I can, but sites with this high concurrency usually
don't rely on a single relational database. To increase the throughput of the data
access layer, a cache for frequently accessed data should be used.
This cache is typically a key-value store, which has much higher throughput and
concurrency than relational databases, since it has much less functionality.
\par
Such a key-value database is Redis, which uses non-blocking IO to handle connections.
Another approach is to store data in the same process as the application accessing
the data. This provides less separation, but shared memory is faster than any
communication over a socket. To simulate caching, I've counted the number of 
requests served, and only accessed the database on every second request.
This translates to a 50\% cache hit ratio, which can be adjusted.
\par
Using this setup I've tried to benchmark the application again. The tests
however were inconclusive. Two consecutive runs of the same test often gave
differences of 10000 request/second, and sometimes the tests didn't even complete,
and the application crashed with a GC overhead limit error, with both synchronous
and asynchronous request handling. As the number of pending connections increased,
the reliability of the tests decreased. Two of the components that should be most
affected by an increase in the number of pending connections is the servlet container
and the connection pool. As of the finishing of this thesis, I could not find out the 
cause of these issues.
\par
I still wanted to evaluate the blocking approach against the non-blocking, 
so I decided to replace the servlet container and the connection pool. 
I wrote a very simple HTTP server using Netty, that was the bare minimum 
approach to serve a request. This server was able to serve 400000 requests
per second without database access.
\par
\begin{figure}[h]
\centering
\includegraphics[scale=0.35]{figures/performance-throughput-chart}
\caption{Throughput numbers for the reliable benchmark}
\label{fig:perf-throughput}
\end{figure}
I replaced the database access with a class called
\ic{Work}, which has a single method called \ic{work()}. The Work class
consists of two queues: A bounded processing queue for workers who called the \ic{work}
method, and a wait queue for workers who could not enter the processing queue,
because it was full. Workers stay in the processing queue for a time equal to the
\ic{workLength} parameter, after which they emit a notification via \ic{Observable.onNext()}
and exit. The members who have waited the longest in the wait queue enter the processing
queue at this point. I also simulated cache effects using the request counting approach
described earlier. For the benchmarks I've set up a work instance with 10000 slots in the
processing queue, and a work length of 500 ms. Cache hit rate was configured to be
90\%.
\par
\begin{figure}[h]
\centering
\includegraphics[scale=0.3]{figures/performance-memory-chart}
\caption{Memory usage numbers for the reliable benchmark}
\label{fig:perf-memory}
\end{figure}
I set up two versions of this test, one where connections were handled via non-blocking
IO, and \ic{work()} was handled asynchronously, and one where connections were non-blocking,
but handling of work was delegated to a fixed thread pool. This is similar to
Tomcat 8's processing model. The results of this benchmark are shown in figures
\ref{fig:perf-memory} and \ref{fig:perf-throughput}.
The term ``blocking 10k'' means the blocking model with 10000 threads.
The JVM's were allowed 512Mb of heap space with the option \ic{-Xmx512m}.
\par
There was no difference in CPU utilization for the tests (it was usually 100\% on all cores). 
The benchmarks show that my particular machine's throughput decreased as the number
of threads grew, and the memory consumption increased. In fact at 40000 threads the
JVM couldn't start more threads, and finished with an error.
The non-blocking model on the other hand scaled well, showing that as the
number of concurrent requests increased, it was able to serve more  of them.
The non-blocking model consumed the smallest amount of memory, as it didn't
have to support thousands of threads with a large initial stack size.
\par
This leaves me to conclude that the non-blocking model indeed scales better
when faced with an unusually high number of concurrent requests, but the tooling
in Java has to mature for the non-blocking model to be applicable in practical
situations. Non-blocking access to relational databases is necessary for the
full stack to be non-blocking, but without good caching, relational databases will not
be able to serve such high number of concurrent requests.