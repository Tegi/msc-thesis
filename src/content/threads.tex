\chapter{Threads}\label{c:threads}

\section{The need for concurrency}
Concurrency and parallelism are two terms that people often confuse.
There is an important distinction, and I cannot put it any better than 
Rob Pike did\cite{rob-pike-talk}:
\par
``\textit{
When people hear the word concurrency they often think of parallelism, 
a related but quite distinct concept. 
In programming, concurrency is the composition of independently executing processes, 
while parallelism is the simultaneous execution of (possibly related) computations. 
Concurrency is about dealing with lots of things at once. 
Parallelism is about doing lots of things at once.
}''
\par
It is clear what concurrency is, but why do we need it? 
It is a well known fact that there is a large difference between the speed of CPUs,
and that of other parts of the computer as seen in table \ref{t:access-times}. 
Let's say that a CPU cycle is one second. 
If a computer had a single program running on it, which wanted to send a TCP packet 
from San Fransisco to New York, the CPU would have to sit idly for 4 years. 
It is therefore logical to have other programs run on the same computer to better 
utilize the CPU. 
The tool to achieve this on Linux are processes.
\begin{table}[]
\centering
\caption{Access times of different targets. Source:\cite{systems-performance}}
\label{t:access-times}
\begin{tabular}{l|l|l|}
\cline{2-3}
                                                & Acess time & If a CPU cycle was 1s \\ \hline
\multicolumn{1}{|l|}{1 CPU cycle}               & 0.3 ns     & 1 s                         \\ \hline
\multicolumn{1}{|l|}{Level 1 cache access}      & 0.9 ns     & 3 s                         \\ \hline
\multicolumn{1}{|l|}{Level 2 cache access}      & 2.8 ns     & 9 s                         \\ \hline
\multicolumn{1}{|l|}{Level 3 cache access}      & 12.9 ns    & 43 s                        \\ \hline
\multicolumn{1}{|l|}{Main memory access}        & 120 ns     & 6 min                       \\ \hline
\multicolumn{1}{|l|}{Solid-state disk I/O}      & 50-150 $\mu$s  & 2-6 days                    \\ \hline
\multicolumn{1}{|l|}{Rotational disk I/O}       & 1-10 ms    & 1-12 months                 \\ \hline
\multicolumn{1}{|l|}{Internet: SF to NYC}       & 40 ms      & 4 years                     \\ \hline
\multicolumn{1}{|l|}{Internet: SF to UK}        & 81 ms      & 8 years                     \\ \hline
\multicolumn{1}{|l|}{Internet: SF to Australia} & 183 ms     & 19 years                    \\ \hline
\end{tabular}
\end{table}

\par
The IO:CPU ratio of a process (or a task that is implemented by a process) is 
the amount of IO-bound work a process has to do divided by the CPU bound parts 
of the process' work. 
\par
This measure is not a definite number, but rather a feel for what a task is like.
For example calculating the SHA-256 hash of an array of bytes has an IO:CPU ratio 
of 0, as no IO is involved, it's entirely done by the CPU. 
Reading a file from a disk has about as high an IO:CPU ratio as possible 
(let's say $\infty$), because the CPU only executes instructions to serve the IO request. 
Compressed video playback would have a low, but not zero IO:CPU ratio.
It has to read files into memory, but also has to compute very CPU-intensive decoding.
Querying a remote PostgreSQL database on the other hand would have a very high value.
It roughly consists of the following steps:
\begin{itemize}
	\item Open a socket to the server and send a protocol initializing message
	\item Receive and parse the answer
	\item Send a query message to the server
	\item Receive and parse the answer
	\item Send a termination message to the server
	\item Return with the result of the query
\end{itemize}

The task moves through a few different states, as IO sections are very rarely 
interrupted by a bit of parsing the answer, and instructing the hardware components 
to send messages to the remote server.
I will refer to this as a split-phase, IO intensive task. The phases are sections of 
code divided by blocking communication. 
I will argue that there are considerable overheads to using threads on workloads of 
a large number of concurrent, high IO:CPU tasks.

\section{A short history of the Linux thread}
The need for concurrency came about as CPU speeds quickly increased, and IO devices 
couldn't keep up the same pace. 
The first solution was using processes, which offered multiple programs running at 
the same time, with a large amount of separation. 
This separation is achieved by processes having separate virtual memory spaces, 
kernel stacks and file descriptor lists amongst other structures. 
Each process may run as if it was the only one on the machine, but they have some 
methods to communicate with each other if they need to.
\par
Maintaining this degree of separation makes some tasks more difficult. 
Having entirely different virtual memory spaces requires the VM subsystem to flush 
the translation lookaside buffer (TLB) each time a new process is scheduled to run. 
Processes did not have communication methods that were satisfactorily quick. 
These are some of the reasons why lightweight processes (LWP) were introduced.
\par
LWPs --- just as processes --- are created by cloning an already existing process. 
Unlike regular processes, LWPs may choose to have less separation from their parents. 
They may inherit the virtual memory space of their direct ancestors 
(along with other structures, like file descriptor tables). 
As the name suggests, this relaxation of separation results in a leaner construct, with less overhead. 
This is why thread libraries, like the current implementation of the POSIX thread 
interface use lightweight processes to provide their services, by assigning threads 
to LWP's at a 1:1 ratio. 
\par
Assigning a single LWP to a thread results in a relatively straightforward 
implementation, as the operating system takes care of much of the difficult problems.
To make communication easier, threads spawned by the same process may read and 
write their collective, shared memory. 
In some opinions\cite{problem-with-threads}, mutable shared state is one of the most 
unsafe ways to handle concurrency, because it introduces race conditions and deadlocks 
due to the nondeterministic activity of the scheduler. 
Nonetheless, this evolution of the Linux process seems natural, and it became the de 
facto way to implement a program with concurrent tasks.
\par

\section{Method of comparison}
An operating system thread is just one of many ways to handle concurrency, 
it makes sense to compare threads with other approaches.
For this comparison, I will consider two aspects of concurrency handling: 
Programming model, and performance.
\par
I am going to demonstrate the programming model on a sample code that represent a typical
case of a high IO:CPU, multiple stage task.
To handle this workload, tasks have to be paused while waiting for IO, and resumed
when the IO operation completes. How the task's state is managed between these
two points in time is crucial, so I take a careful look at it.
The other important thing for a task management approach is to be resource efficient.
For this reason I will look at how the model performs compared to others.
\par
Two prominent challenges of handling concurrency is synchronization and communication. 
In my thesis I will refrain from discussing these issues in depth, 
as  the main scenario is accessing a relational database. 
Relational database engines are excellent at managing shared mutual state, 
and coordinating transactions. 
So much so, that transaction management is part of the SQL standard. 
Therefore I will consider applications that defer these problems to the RDBMS
where possible.

\section{Programming model} 
The TIOBE index's top 5 programming languages (Java,C,C++,C\#,Python) support
handling concurrent tasks using operating system threads by providing standard library
functions. IO operations are usually implemented by delegating to the operating 
system's blocking functions. Non-blocking methods were only intoruced later\footnote{
As an example, standard non-blocking operations were only introduced 
in the 7th major release of Java
}.
For these reasons I consider threads the default way to handle concurrent tasks.
\par
To compare this method with others, I introduce a part of a program that I consider 
a typical case for a high IO:CPU task. I will use this to exhibit the programming model 
of different solutions for handling concurrency. The source code for this program 
can bee seen in listing \ref{lst:thread-example}.

\begin{listing}
\caption{The example program implemented with operating system threads}
\label{lst:thread-example}
\begin{minted}[linenos]{Java}
Database db = new Database();
try {
    String cartId = request.cookies.get("sessionId");
    Cart cart = db.findCart(cartId);
    for(Item item : cart.items) {
        db.decreaseItemStock(item);
    }
    db.savePurchase(cart.items, new Date());
} catch(Throwable t) {
    log.error("Purchase failed", t);
    redirectToErrorPage();
}
\end{minted}
\end{listing}

\begin{listing}
\caption{Blocking interface for the database of the sample program}
\label{lst:thread-example-database}
\begin{minted}[linenos]{Java}
interface Database {
    Cart findCart(String id);
    void decreaseItemStock(CartItem item);
    void savePurchase(List<CartItem> items, Date ts);
}
\end{minted}
\end{listing}

This sample program is part of a web shop. It handles a consumer's request
for buying the contents of their cart. It is implemented as a sequence of steps:
\begin{itemize}
\item Retrieve the customer's cart containing items from the database using a cookie 
from the HTTP request.
\item Decrease the stock for each item.
\item Save the items with the current timestamp in the database for later retrieval.
\item If an error should occur, log the error, and redirect the user to the error page.
 This is usually not implemented this way, but for the sake of this example I presume 
 that there is no default exception handler that would do this.
\end{itemize}

As seen in listing \ref{lst:thread-example-database}, the database is implemented in a blocking 
fashion. Each method contacts a PostgreSQL database engine, submits a query, and waits 
for it's result. Communication is handled by JDBC, so each method puts the calling 
thread into a waiting state while reading and writing from a TCP socket.

\subsection{State management}\label{s:thread-state-management}
As I have shown earlier, split-phase, high IO:CPU ratio tasks consist of the following 
phases:
\begin{itemize}
\item Request some IO resource (e.g. listening for a TCP connection)
\item Wait for the result
\item A relatively short amount of work on the CPU (e.g. processing an incoming connection)
\end{itemize}
These phases may repeat an arbitrary number of times in a task. The state of a task
has to be preserved, so it can continue running when the result of an IO operation
arrives. Therefore the details of this state management is crucial for an
efficient implementation.
\par
Linux threads share the address space of their parents, but they each have a 
separate stack, and process data structure using which they maintain their state 
while waiting for IO completion.
\par
When a thread has to wait for an IO operation, the value of the instruction pointer (IP)
is saved, and the thread is put on a wait queue for the specified IO event. 
When it gets scheduled again, the instruction pointer and the thread's stack is 
restored, and execution resumes at the next command.
\par
\begin{figure}[h]
\centering
\includegraphics[scale=0.7]{figures/callStack}
\caption{A new stack frame is created for each function call}
\end{figure}
In the JVM, the stack is divided into stack frames. Each stack frame contains 
an array of local variables, which can later be referenced by indexing.
A Java compiler would translate access to the \ic{cart} variable into accesses 
to the local variable array. This mechanism allows storage of partial results.
\par
Let's consider the line \ic{Cart cart = db.findCart(cartId)} in listing \ref{lst:thread-example}. 
This line introduces a local variable, and assigns the return value of a function 
to it. The value of the \ic{cart} variable is accessible until the function it was 
declared in returns. The task may block as many times as necessary, the value of 
\ic{cart} remains accessible each time the task resumes.
\par
Each time a function is called, a new stack frame is created. Arguments to this function 
call (as well as it's return value) are passed via a part of the stack frame called 
the \textit{operand stack}. 
When the end of the function is reached, and the function returns, the stack frame
is destroyed.
At any point, we can inspect our task (now implemented by a 
thread) to see which is the currently executing function, which function invoked that 
function, and so on. This is called the stack trace.
\par
Listing \ref{lst:stacktrace} shows part of a high level view of the 
HotSpot Java virtual machine stack of one thread, stopped by the debugger. 
Read from the bottom towards the top, it shows which functions are executed
by the thread, and the invocation relationships between those functions.
Should the program fail, a similar stack trace can be stored, and later examined
to see the state that lead to the failure.

\begin{listing}
\caption{Stack trace of a thread in the Java Virtual machine}
\label{lst:stacktrace}
\begin{lstlisting}
"http-nio-8080-exec-1@7462" daemon prio=5 tid=0x25 nid=NA runnable
at hu.tegi.bookstore.CatalogController.index
at org.grails.web.mapping.mvc.UrlMappingsInfoHandlerAdapter.handle
at org.springframework.web.servlet.DispatcherServlet.doDispatch
at org.springframework.web.servlet.DispatcherServlet.doService
at org.springframework.web.servlet.FrameworkServlet.processRequest
at org.springframework.web.servlet.FrameworkServlet.doGet
at javax.servlet.http.HttpServlet.service
at org.springframework.web.servlet.FrameworkServlet.service
at javax.servlet.http.HttpServlet.service
at org.apache.tomcat.websocket.server.WsFilter.doFilter
at org.springframework.boot.actuate.trace.WebRequestTraceFilter.doFilterInternal
at org.grails.web.servlet.mvc.GrailsWebRequestFilter.doFilterInternal
at org.grails.web.filters.HiddenHttpMethodFilter.doFilterInternal
at org.springframework.web.filter.CharacterEncodingFilter.doFilterInternal
at org.springframework.boot.actuate.autoconfigure.MetricsFilter.doFilterInternal
at org.apache.catalina.core.StandardWrapperValve.invoke
at org.apache.catalina.core.StandardContextValve.invoke
at org.apache.catalina.authenticator.AuthenticatorBase.invoke
at org.apache.catalina.core.StandardHostValve.invoke
at org.apache.catalina.valves.ErrorReportValve.invoke
at org.apache.catalina.core.StandardEngineValve.invoke
at org.apache.catalina.connector.CoyoteAdapter.service
at org.apache.coyote.http11.AbstractHttp11Processor.process
at org.apache.coyote.AbstractProtocol$AbstractConnectionHandler.process
at org.apache.tomcat.util.net.NioEndpoint$SocketProcessor.doRun
at org.apache.tomcat.util.net.NioEndpoint$SocketProcessor.run
at java.util.concurrent.ThreadPoolExecutor.runWorker
at java.util.concurrent.ThreadPoolExecutor$Worker.run
at org.apache.tomcat.util.threads.TaskThread$WrappingRunnable.run
at java.lang.Thread.run
...
\end{lstlisting}
\end{listing}

To manage the state of a task, a task handling method should implement two distinct features:
\begin{itemize}
\item Storing and retrieving partial results
\item Keeping track of execution state
\end{itemize}
JVM threads implement execution state and partial result storage via function calls 
and local variables, and these are in turn implemented by the stack and some special 
registers. Note however that this is just one way to realize these components.
As an example common assembly languages have no function calls, or local variables, 
but with clever memory management, and use of jump statements they can implement 
the same functionality.

\subsection{Task context}\label{s:task-context}
Threading systems are typically capable of introspection. 
That is to say, there is an instruction to query the id of the currently executing 
thread. Using this in conjunction with memory areas attached to each thread, it is
not difficult to create a context, that is available throughout the lifetime of a 
thread, and is easily accessible for any code executing as part of the task implemented
by the thread. In Java, a variable attached to a thread is called a \ic{ThreadLocal}.
\par
ThreadLocal variables are sometimes used by frameworks to establish a sort of global 
variable, that has different values depending on the calling thread.
An example of this is Spring MVC's \ic{RequestContextHolder} class, which holds references
to the servlet request and response objects. 
This is achieved through static methods, which access static variables of the class.
This leads to global variables, as there is only one \ic{RequestContextHolder} class in the 
application\footnote{
To be technically correct, there may be one \ic{RequestContextHolder} per \ic{ClassLoader},
but this is irrelevant in this situation.
}. Due to the fact that these variables are ThreadLocal, each thread will see a separate
value referenced by this variable.
\par
In the thread-per-request servlet model, each request is served by one\footnote{
Sometimes worker threads help processing, but these typically don't need access to the
servlet request.
} thread. This way all code that handles the task has access to the servlet request,
without any other component having to assign it to them.
\par
As I've just shown, use of ThreadLocals --- which are analogous to global variables ---
can be convenient. The current request is accessible throughout the processing pipeline.
This might seem unnecessary, after all object fields and function arguments are 
adequate tools to pass variables from one lexical scope to another.
On the other hand, the request may be necessary in situations where the object in
question isn't instantiated by us, or the signature of the method is fixed. 
In these situations a global variable may be preferable to other types of workaround.
\par
ThreadLocals are not without their dangers. If the thread is not destroyed after
the task has finished, the ThreadLocal context may cause memory leaks. 
This is a real danger, as  threads are usually pooled for performance reasons, 
as I will show in section \ref{s:threads-performance-creation}.
\par
Classes using global variables (or public static ThreadLocals) are tightly coupled
to the class declaring these global variables. If an implicit task context is used
throughout the application, it becomes even more difficult to change the class
declaring the global variables. If the context was passed through a method,
or constructor argument, it would be much easier to replace it.
\par
Code accessing public static fields of other classes is much harder to test than 
code realizing this functionality through method arguments or private fields. 
This is because replacing public static fields of a class with mock implementations 
for the duration of one particular test is especially challenging.
It makes the global variable accessing code much harder to test in isolation,
which is the crux of modern testing methodologies.
\par
I would advise against using ThreadLocals as global context variables when not necessary,
but the fact is that large frameworks, such as Spring MVC use them as a core part 
of their infrastructure. However I see the appeal of variables attached to tasks,
as they can be convenient in certain situations. To reduce coupling, I believe
access to global state should be isolated, and covered behind well defined interfaces.

\subsection{Scheduling}
Making the choice of which process should execute next on the CPU is 
referred to as scheduling. The operating system has to choose the next 
thread in two cases:
\begin{itemize}
\item The current gets blocked, because it requests a resource that is 
not immediately available. In this case the scheduler puts the process 
on a wait queue to be made runnable later, and selects a task that is 
not waiting right now. 
The current process may also yield the processor, in which case the 
process remains runnable, but offers the scheduler to select another 
task instead of it. 
These two sets of circumstances are referred to as voluntary switches, 
because the scheduling is triggered by an instruction that the process requested.
\item A predefined amount of time has expired, and the scheduler interrupts the 
current process, to assign the processor to another thread. 
This is called involuntary switching, as it was not initiated by the currently executing task.
\end{itemize}
Systems relying on voluntary process switches entirely are called cooperative, 
because tasks have to work together, allowing each other to run by yielding the 
processor (explicitly or implicitly). 
This is a risky strategy, as a poorly constructed program --- which never yields --- 
will starve other threads by denying them processor time. 
Arrangements utilizing involuntary thread switches are called preemptively scheduled systems, 
which can arrange multiple CPU intensive processes without requiring cooperation.
\par
Linux has a preemptive scheduling system with an abundance of useful features. 
Processes can be assigned priorities, and the scheduler tries to allocate fair amounts 
of CPU time to each process with respect to their priorities. The scheduler also 
performs load balancing between CPUs. 
It tries to keep each CPU equally loaded, for which it might need to migrate a process 
from one processor to the other. 
The scheduler preempts at a frequency that can be tuned. The frequency of a modern, 
general purpose Linux kernel is 100 Hz. 
A very detailed description on how all these features are provided by the Linux 
kernel can be found in\cite{understanding-the-linux-kernel}.
\par 
For tasks with very short bursts of CPU usage, fair scheduling is not that important, 
as they will not occupy the CPU for too long, and starve other processes. 

\section{Performance}
In this section I am going to describe what ``overheads'' threads entail. 
Most of the sources I've read mention that at high levels of concurrency, using 
a thread per task is too much of an overhead, but they almost never care to explain 
what these overheads are. 
They occasionally mention context switches and memory issues vaguely, but I've not 
read one that clarifies what this exactly means.

\subsection{Creation}\label{s:threads-performance-creation}
Creating a new process consists of the kernel cloning the parent process, 
then initializing the new process' user and kernel mode structures (stacks, descriptors). 
For a detailed discussion on how this is implemented, see\cite{understanding-the-linux-kernel}.
\par
This procedure is of course very well optimized, but creating a new process every 
time a task needs to be executed would be relatively slow. 
A technique to work around this time penalty is to create a thread pool.
\par
The pool has a set of worker threads, optionally a coordinator thread, and a queue 
for submitted tasks. 
The threads are waiting for work to be assigned to them, after which they start 
executing the task. 
Once the thread is finished, it returns to the pool, to wait for another assignment. 
A thread pool is an easy concept, but choosing wrong data structures for the 
implementation can lead to poor performance.
\par
This approach eliminates the issue of having to create a process for each task the program 
is given.
All of the threads are created at the start, and maintained by the pool. 
These threads still have to be created, so there is a startup time penalty, which 
is usually not an issue with server applications. 
A user of thread pools should also choose the minimum amount of threads that is 
sufficient, because even idle pool members have overheads as discussed in the next section.

\subsection{Memory}
The default stack size on my personal computer right now is 8 megabytes. A common misconception is that if one thread per task was used, serving 10 000 users at the same time would mean using about 80 gigabytes of memory. This is clearly unacceptable, but fortunately not the case. Like many operating systems, the Linux kernel implements demand paging, which means that --- although virtual memory is allocated --- physical memory is not occupied until the page is accessed. 
\par 
A simple demonstation of demand paging can be seen in listing(\ref{lst:demand-paging}). If we set the \texttt{ASIZE} parameter to 1, each of the five tasks will need much less than 4 kilobytes(a single page). Running Linux utility \texttt{pmap -x <PID>} shows that 8 kilobytes (two pages) of physical memory is actually occupied, with 8192 kilobytes of virtual memory committed. If we set \texttt{ASIZE} to 4097, this should trigger one more page to be occupied, which is exactly the case, as pmap's output shows in listing(\ref{lst:pmap-3pages}).

\begin{listing}
\caption{A simple program demonstrating demand paging}
\label{lst:demand-paging}
\begin{minted}[linenos]{C}
#define ASIZE 4097

void task() {
  unsigned int i;
  unsigned char a[ASIZE];
  for(i = 0; i < ASIZE; i++) {
    a[i] = 0;
  }
}

int main(void) {
  int numProc = 5, i;
  for (i = 0; i < numProc; i++) {
    pthread_t tid;
    int err = pthread_create(&tid, NULL, &task, NULL);
    ...
  }
}

\end{minted}
\end{listing}

\begin{listing}
\caption{pmap's output showing that only 12 kilobytes of physical memory is used of the 8192 kilobytes of virtual memory when running the program seen in listing(\ref{lst:demand-paging})}
\label{lst:pmap-3pages}
\begin{lstlisting}
Address           Kbytes     RSS   Dirty Mode  Mapping
...
00007f22bc0c5000       4       0       0 -----   [ anon ]
00007f22bc0c6000    8192      12      12 rw---   [ anon ]
00007f22bc8c6000       4       0       0 -----   [ anon ]
00007f22bc8c7000    8192      12      12 rw---   [ anon ]
00007f22bd0c7000       4       0       0 -----   [ anon ]
00007f22bd0c8000    8192      12      12 rw---   [ anon ]
00007f22bd8c8000       4       0       0 -----   [ anon ]
00007f22bd8c9000    8192      12      12 rw---   [ anon ]
00007f22be0c9000       4       0       0 -----   [ anon ]
00007f22be0ca000    8192      12      12 rw---   [ anon ]
...
\end{lstlisting}
\end{listing}

\par
The stack space overhead affects 32 bit systems much more severely, as they only have about 3 gigabytes of addressable memory. Using 8 megabyte stacks, the virtual memory space would quickly be depleted. These systems have the option of using split stacks\cite{split-stacks}. There are also systems where virtual memory isn't implemented (for example some embedded systems). In my opinion servers aiming to serve thousands of users should use 64 bit addresses with virtual memory.
\par
Other than the userspace stack, threads need some kernel memory, which should also be accounted for. Each thread's kernel mode stack and thread info structure takes up at least two pages according to\cite{understanding-the-linux-kernel}.
\par
Stacks are clearly valuable. They store a thread's state, and part of the execution history of the current method call. Stacks also provide a very simple storage area. Unlike heap memory, stack space doesn't get segmented, thereby allocations are resolved faster. On the other hand, split-phase, high IO:CPU workloads have a large number of idle tasks waiting for IO completion. Storing all of their execution states in stacks might be considered costly.

\subsection{Scheduling}
Whether it's a cooperative or a preemptive system, split-phase workloads with high 
IO:CPU ratios need to perform a lot of context switches. 
By definition they consist of short sections of work on the CPU, after which they 
wait for the next IO operation to complete. 
For this reason switching from one task to another should take the least time possible.
\par
As seen in table(\ref{t:access-times}) accessing the processor cache is at least 
an order of magnitude faster than accessing main memory. 
Processes with good spatial and temporal locality can utilize the CPU cache very well. 
The CPU cache is an immensely complicated topic, so instead of discussing it in detail,
I would refer the reader to the excellent source on it\cite{what-every-programmer-should-know-about-memory}. 
An often cited overhead of thread switching is that it ruins the CPU cache. 
This is true to some extent, but I think an important clarification is that it's not 
thread switching that makes the cache less effective, but rather the way thread 
switching is implemented, and the poor spatial locality of threads as a group.
\par
Let's imagine an ideal scheduling algorithm that is able to choose the next running 
task and restore it's hardware context without accessing main memory, thereby 
not changing the CPU cache. 
The next thread should only have cache misses, if it accesses memory that was not 
used by the previous thread, or only cache hits, if it accesses the same data\footnote{
This is when the current and next processes are both lightweight processes. 
Normal processes do not share virtual memory space
}. As we can see, this particular ``scheduling algorithm'' does not affect the cache.
In reality Linux's scheduling algorithm is a sophisticated, but complicated procedure 
which uses memory that is potentially not in cache, and therefore needs to replace some
other cache content. 
I suppose, programs could be constructed that would fit entirely in cache, 
along with the Linux scheduling algorithm, but I question the applicability of such a program. 
My point is that cache misses will occur even with an ideal scheduling algorithm, although 
they will be less frequent the closer the procedure is to the ideal.
\par
An empirical method to measure the time it takes to perform a context switch 
is attempted by \cite{ctx-switch-measurement}. 
It tries to construct a program that does nothing else, but switch context a 
number of times (or tries to get as close to this as possible). 
Timing how long this program takes to execute divided by the number of switches 
should give the amount of time required for one context switch. 
On my personal computer this benchmark yielded about 1,1$\mu$s/context switch. 
This number depends on the CPU architecture and the OS scheduling implementation, 
but common reported values are between 1 and 5$\mu$s. 
This is the direct cost of context switching. 
The indirect cost (the amount of time lost due to cache misses due to cache memory 
being displaced by the scheduler) is much harder to measure, as it is almost entirely 
dependent on the program being executed.
\subsection{Summary}
Using the theories and measurements presented in this chapter, it can be concluded that 
highly concurrent, split-phase, high IO:CPU programs implemented with threads are affected 
by two main types of overhead:
\begin{itemize}
\item The state storage implemented by threads seems inefficient and inflexible. 
Stacks can only grow by well defined amounts, and the system default is potentially wasteful. 
A high number of idle tasks (waiting for IO completion) will occupy large amounts of memory, 
which they might not need. Using threads there is no way to opt out of this state storage.
\item A typical task uses the processor for a short amount of time, after which it needs 
to wait for IO, which necessitates choosing a new task to run. 
Linux selects the next thread using a preemptive, feature-rich algorithm, and switches 
it with the current thread respecting their hardware contexts necessary for the suspendability 
of threads. 
The cost of this abundance of features is a relatively slow execution speed compared to 
the amount of thread switches per second required by the type of workload for which we want to optimize.
\end{itemize}
A general approach to any problem has much less room to optimize for a particular workload. 
The Linux threading system is a general approach to handle concurrency. 
It has to perform relatively well on a number of workloads: 
serving websites, compiling programs, playing multimedia, manipulating 3D models, etc. 
Therefore it is my strong opinion that all of the ``overheads'' presented are not a product 
of poor programming, but rather necessary to facilitate all of the use cases listed above. 
I believe that a version of the Linux kernel could be implemented with even lighter-weight 
processes, and much simpler scheduling. 
Split-phase, high IO:CPU tasks would perform much better on this version, but it would 
render the system near-useless for other programs. 
Changing the OS to suit a particular workload is a radical step. 
This is why alternative forms of concurrency handling --- outside the scope of the operating system --- 
should be considered.
\par
Although threads may not scale as well as necessary for these types of workloads,
they have considerable benefits. Programming with operating system threads is very popular, 
as many of the most used programming environments implement this type of task handling.
Threads have impressive state storage, that enables a seamless transition between 
task stages where the thread is paused, and then scheduled to run again.

\section{A note on the Java Virtual Machine}
I've used the JVM to create the program for my thesis, therefore I will comment on
the JVM more than any other programming environment. To be more precise, I will 
examine the HotSpot implementation of the Java Virtual Machine. This is only a 
particular implementation of the specification, but it is used in the 
overwhelming majority of cases according to \cite{jvm-version-statistics}.
\par
This way I can simply say, that each JVM thread maps to a operating system 
thread, and the JVM uses the operating system thread's stack to store 
stack frames. While this is not specified in the JVM specification\cite{JVM-specification},
it is true for HotSpot.

\section{Summary}
In this chapter I've reviewed how operating system threads provide features
related to task handling. 
Chief amongst these is state management, as this is what allows tasks to be suspended, 
and later resumed. 
Although operating system stacks provide a remarkable abundance of tools, they seem
to fall short in efficiency when faced with an enormous amount of tasks.
\par
Task context, or task-global variables may be considered optional, as they are not 
responsible for any essential task component, but they allow for some convenient workarounds.
This feature is not especially detrimental to performance, but there are other 
dangers that have to be considered when using them.
\par
Scheduling is a part of any task management system capable of handling concurrent tasks.
General purpose operating systems must consider a multitude of task models, and not
just the one presented in this thesis. This is why thread scheduling is not as efficient
as necessary for this kind of work.
\par
Examining operating system threads is very important, as --- due to it's popularity ---
this is the baseline to which I will compare other solutions.